const { createApp } = Vue;
createApp({
  data() {
    return {
      randomindex: 0,
      randomindexinternet: 0,

      //vetor de imagens locais
      imagenslocais: [
        "./imagem/lua.jpg",
        "./imagem/SENAI_logo.png",
        "./imagem/sol.jpg",
      ],
      imagensinternet: [
        "https://conteudo.imguol.com.br/c/esporte/8d/2022/09/22/mbappe-abre-o-placar-para-a-franca-contra-a-austria-na-liga-das-nacoes-1663879275493_v2_450x450.jpg",
        "https://p2.trrsf.com/image/fget/cf/774/0/images.terra.com/2022/10/31/neymarjr_306185594_1466211463885538_1633088772187345691_n-1jyb0w6t08rhz.jpg",
        "https://p2.trrsf.com/image/fget/cf/1200/1200/middle/images.terra.com/2022/12/24/1414215821-agenciacorinthians-foto-196178.jpg",
      ],
    }; //fim return
  }, // fim data
  computed: {
    randomimagem() {
      return this.imagenslocais[this.randomindex];
    }, // fim randomimagem
    randomimageinternet() {
      return this.imagensinternet[this.randomindexinternet];
    }, //fim randominternet
  }, // fim comput
  methods: {
    getrandomimagem() {
      this.randomindex = Math.floor(Math.random() * this.imagenslocais.length);
      console.log(this.imagenslocais[this.randomindex]);

      this.randomindexinternet = Math.floor(
        Math.random() * this.imagensinternet.length
      );
    },
  }, //fim methods
}).mount("#app");
